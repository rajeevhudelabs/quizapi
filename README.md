![Logo](http://tribeout.com/hudelabs-logo.png)  

# Quiz API  
Quiz API Enables users to fetch questions categorized into categories and topics.  
## Getting Started  
These instruction will get you a copy of project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
### Prerequisites
What things to needs to install the software and how to install them. You will need [Node.js >= 7.0.0](www.nodejs.com) and [NPM >=4.0.0](www.nodejs.com)  
Run the following commands to setup a local copy of the software.  
```sh 
git clone https://github.com/pareshchouhan/test-git.git 
```

### Installing  
 To get your server up and running run the following  commands, make sure you have cloned the [repository](www.github.com) before proceeding.

```sh
cd test-git
npm install
npm start
```  
Wait until you see the following output
```sh
INFO    -   Building documentation...
INFO    -   Cleaning site directory
[I 170309 16:07:05 server:283] Serving on http://127.0.0.1:8000
[I 170309 16:07:05 server:283] Serving on http://127.0.0.1:8000
[I 170309 16:07:05 server:283] Serving on http://127.0.0.1:8000
```
Now open a web browser and run ```http://hudelabs.local/phpmyadmin``` to see if everything is up and running.

## Running the test  
To run the tests, run the following commands
```sh
npm install
npm run test
```
```sh
var i = 0;
for(i=30 ; i > 0 ; i--) {
    console.log('It Works');
}
```

## Deployment

* Make Sure you have [Node.js](www.nodejs.com) installed
* Requires Kernel version 4.4.0
* Requires php-fpm installed
* requires php-7.1

## Build With

* [Dropwizard][1] - The web framework used
* [Maven][1] - Dependency Management
* [Rome][1] - Used to generate RSS feeds

## Versioning  
We use [SemVer][1] for versioning. For the versions available, see the [tags on this repository][2].  

## Authors  
* **Your name here** - Initial work - [Your Name Here][3]

See also the list of [contributers][3] who participate in this project.  

## License  

This project is licensed uder the MIT License - See the [LICENSE.md][3] for file details  

## Acknowledgements  
* Hat tip to anyone who's code was used
* inspiration  
* etc 
 

[1]: <http://www.hudelabs.com>
[2]: <https://www.github.com>
[3]: <https://www.gitlab.com>



 
 